import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: boolean = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: number) {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: boolean = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
