import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

class App {
  static rootElement: HTMLElement | null = document.getElementById('root');
  static loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

  constructor() {
    this.startApp();
  }

  async startApp(): Promise<void> {
    try {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'visible';
      }
      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);
      if (App.rootElement) {
        App.rootElement.appendChild(fightersElement);
      }
    } catch (error) {
      console.warn(error);
      if (App.rootElement) {
        App.rootElement.innerText = 'Failed to load data';
      }
    } finally {
      if (App.loadingElement !== null) {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
}

export default App;
