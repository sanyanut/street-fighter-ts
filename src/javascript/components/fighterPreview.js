import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter){
    const ul = document.createElement('ul');
    const list = `
      <li>Name: <b>${fighter.name}</b></li>
      <li>Health: ${fighter.health}</li>
      <li>Attack: ${fighter.attack}</li>
      <li>Defense: ${fighter.defense}</li>
    `;
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(ul);
    ul.setAttribute('class', 'fighter-preview__info');
    ul.innerHTML = list;
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
